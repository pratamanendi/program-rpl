/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    'index.html',
    './assets/swiper/swiper-element-bundle.min.js'
  ],
  theme: {
    extend: {
      colors: {
        blue: {
          900: '#131E44'
        },
        red: {
          100: '#FFEBED',
          900: '#9F2937'
        },
        slate: {
          50: '#FFEBEF',
          300: '#E6E6E6'
        },
        green: {
          50: '#c0d4d3',
          100: '#2d8984',
        }
      }
    },
    fontFamily: {
      'display': ['"Abhaya Libre"', 'serif'],
      'body': ['"Montserrat"', 'sans-serif'],
    },
    backgroundSize: {
      'auto': 'auto',
      'cover': 'cover',
      'contain': 'contain',
      '40%': '40%',
      '120%': '120%',
    }
  },
  plugins: [],
}
